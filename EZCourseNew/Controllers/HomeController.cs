﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EZCourseNew.Models;
using MailKit.Net.Smtp;
using MimeKit;
using EZCourseNew.Services;
using Microsoft.Extensions.Options;


namespace EZCourseNew.Controllers
{
    public class HomeController : Controller
    {
        readonly Services.Smtp _smtpService;
        readonly ContactOptions _contactOptions;

        public HomeController(Services.Smtp smtpService, IOptions<ContactOptions> contactOptions)
        {
            _smtpService = smtpService;
            _contactOptions = contactOptions.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            //ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpPost]
        public IActionResult Contact(Contact formData)
        {
            if (!ModelState.IsValid)
            {
                return View(formData);
            }
            //return Json(formData);
            //return View();
            //1.Do something
            var htmlBody = $"<p>{formData.Name} ({formData.Email})</p><p>{formData.Phone}</p><p>{formData.Message}</p>";
            var textBody = "{formData.Name} ({formData.Email}) \r\n{formData.Phone}\r\n{formData.Message}";
            _smtpService.SendSingle("Contact Form", htmlBody, textBody,
                _contactOptions.ContactToName, _contactOptions.ContactToAddress,
                _contactOptions.ContactFromName, _contactOptions.ContactFromAddress);
                //2.Set a message
                TempData["Message"] = "Thank you! Your message is sent to us!";
            //3.Redirect the browser.
            return RedirectToAction("Contact"); //Redirect the user back to the Contact Form
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
